import React from 'react'
import { render } from 'react-dom'
import { InertiaApp } from '@inertiajs/inertia-react'
import '../css/app.css'

const el = document.getElementById('app')

render(
    <InertiaApp
        // Pass props from the server down to the client app
        initialPage={JSON.parse(el.dataset.page)}
        initialComponent = {''}
        // Dynamically load the required page component
        resolveComponent={(name) => import(`./Pages/${name}`).then((module) => module.default)}
    />, el
)